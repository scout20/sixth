<!DOCTYPE html>
<html lang="ru">
   <head>
      <style>
         /* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
         .error {
         border: 5px outset red;
         }
      </style>
      <meta charset="utf-8">
      <title>Lab6 Table</title>
      <link rel="stylesheet" href="admin_style.css">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      <meta name="viewport" content="width=device-width, initial-scale=1">
   </head>
   <body>
      <p>Администратор, Вы успешно авторизовались и видите защищенные паролем данные.</p>
      <br>
      <?php
         if (!empty($_COOKIE['id_error'])) {
             echo '<div class = "error">Такого ID в базе нет.</div>';
             setcookie('id_error', '', 1000);
         }
         ?>
      <form class="forma_user mt-6" action="admin.php" method="POST">
         <table>
            <tr class="mycolor">
               <td>ID</td>
               <td>Имя</td>
               <td>Почта</td>
               <td>ДР</td>
               <td>Пол</td>
               <td>Кол-во конечностей</td>
               <td>Сверхспособности</td>
               <td>Биография</td>
               <td>Контракт</td>
            </tr>
            <?php
               foreach ($values as $j) {
                   echo '<tr>';
                   for ($i = 0; $i < 9; $i++) {
                       echo '<td>';
                       print_r ($j[$i]);
                       echo '</td>';
                   }
                   echo '</tr>';
               }
               
               //  print('<pre>');
               //  print_r($values);
               //   print('</pre>');
               
               ?>
         </table>
         <br>
         <br>
         <label> Введите ID пользователя, которого необходимо удалить из базы:
         <br>
         <br>
         <input name="id_delete" value="0" />
         </label>
         <br>
         <br>
         <div class="form-group">
            <input type="submit" class="button" value="УДАЛИТЬ" />
         </div>
      </form>
   </body>
</html>