<?php

// Получение логина и пароля админа из БД.
$user = 'u15645';
$mypass = '3395578';
$db = new PDO('mysql:host=localhost;dbname=u15645', $user, $mypass, array(PDO::ATTR_PERSISTENT => true));
try {
    $stmt = $db->prepare("SELECT * FROM admin_lab6");
    $stmt->execute();
}
catch(PDOException $e) {
    print ('Error : ' . $e->getMessage());
    exit();
}

$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
$login_admin = $rows[0]['login'];
$passmd5_admin = $rows[0]['pass'];


// Пример HTTP-аутентификации.
// PHP хранит логин и пароль в суперглобальном массиве $_SERVER.
if (empty($_SERVER['PHP_AUTH_USER']) || empty($_SERVER['PHP_AUTH_PW']) || $_SERVER['PHP_AUTH_USER'] != $login_admin || md5($_SERVER['PHP_AUTH_PW']) != $passmd5_admin) {
    header('HTTP/1.1 401 Unanthorized');
    header('WWW-Authenticate: Basic realm="My site"');
    print ('<h1>401 Требуется авторизация.</h1>');
    exit();
}
// print('Администратор, Вы успешно авторизовались и видите защищенные паролем данные.');

// Теперь нужно прочитать отправленные ранее пользователями данные и вывести в таблицу.
// Реализовать просмотр и удаление всех данных.

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $stmt = $db->prepare("SELECT id, name, email, date, gender, kolvo, superpower, bio, contract FROM forma_lab6");
    $stmt->execute();
    $user_data_ALL = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    $values = [];
    foreach ($user_data_ALL as $user_data) {
        $hisid = !empty($user_data['id']) ? $user_data['id'] : '';
        $name = !empty($user_data['name']  || preg_match('/^[а-яА-ЯёЁa-zA-Z ]+$/u', $user_data['name'])) ? strip_tags($user_data['name']) : '';
        $email = !empty($user_data['email']) ? strip_tags($user_data['email']) : '';
        $date = (!empty($user_data['date']) || preg_match('/[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])/u', $user_data['date'])) ? $user_data['date'] : '';
        $gender = !empty($user_data['gender']) ? $user_data['gender'] : '';
        $kolvo = !empty($user_data['kolvo']) ? $user_data['kolvo'] : '';
        $bio = !empty($user_data['bio']) ? strip_tags($user_data['bio']) : '';
        $contract = !empty($user_data['contract']) ? 'yes' : '';
        $superpower = !empty($user_data['superpower']) ? $user_data['superpower'] : '';
        $values[$user_data['id']] = [
            $hisid, 
            $name, 
            $email, 
            $date, 
            $gender, 
            $kolvo, 
            $superpower, 
            $bio, 
            $contract 
        ];
    }
    
    $kolvo_user = count($values);
    include ('table.php');
} 
else {
    $id_delete = [$_POST['id_delete']];
    
    // Проверяем, есть ли такой айдишник в базе данных:
    $user = 'u15645';
    $mypass = '3395578';
    $db = new PDO('mysql:host=localhost;dbname=u15645', $user, $mypass, array(PDO::ATTR_PERSISTENT => true));
    
    // Подготовленный запрос.
    try {
        $stmt = $db->prepare("SELECT * FROM forma_lab6 WHERE id = ?;");
        $stmt->execute($id_delete);
    }
    catch(PDOException $e) {
        print ('Error : ' . $e->getMessage());
        exit();
    }
    
    $user_d = $stmt->fetchAll(PDO::FETCH_ASSOC);   //массив, в к-ый сохраняется полученная строка из БД.
    if (!empty($user_d)) {
        
        // Если все ок, то удаляем из БД строку с этим айди.
        $stmt = $db->prepare('DELETE FROM forma_lab6 WHERE id = ?');
        $stmt->execute($id_delete);
        setcookie('save', '1');
        
        // Делаем перенаправление.
        header('Location: admin.php');
    } 
    else {
        setcookie('id_error', 1, 0);   //ошибка о вводе неверного айдишника.
        header('Location: admin.php');
    }
}
?>